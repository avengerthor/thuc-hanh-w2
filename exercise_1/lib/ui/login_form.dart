import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';

class login extends StatefulWidget {
  createState() {
    return LoginScreenState();
  }
}

class LoginScreenState extends State<login> {
  final formKey = GlobalKey<FormState>();

  Widget build(context) {
    return Container(
        margin: EdgeInsets.all(25.0), //25pixel
        child: Form(
          key: formKey,
          child: Column(
            children: [
              emailField(),
              firstNameField(),
              lastNameField(),
              dobField(),
              addressField(),
              submitBTN(),
            ],
          ),
        ));
  }

  Widget emailField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Email Address',
        hintText: '@gmail.com',
      ),
      validator: (value) {
        if (value != null) if (!value.contains('@')) {
          return "Your email is not valid!!!";
        }
        return null;
      },
      onSaved: (value) {
        print(value);
      },
    );
  }

  Widget firstNameField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'First name',
        hintText: 'Bill',
      ),
      validator: (value) {
        if (value != null) if (value.length < 2) {
          return "Please enter your first name correctly!!!";
        }
        return null;
      },
    );
  }

  Widget lastNameField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Last name',
        hintText: 'Gates',
      ),
      validator: (value) {
        if (value != null) if (value.length < 2) {
          return "Please enter your last name correctly!!!";
        }
        return null;
      },
    );
  }

  Widget dobField() {
    return TextFormField(
      keyboardType: TextInputType.datetime,
      decoration: InputDecoration(
        labelText: 'Enter year of birth',
        hintText: 'Your year of birth',
      ),
      validator: (value) {
        if (value != null) if (value.length < 4 || int.parse(value) > 2022 && int.parse(value) is int) {
          return "Please enter a valid year of birth";
        }
        return null;
      },
    );
  }

  Widget addressField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        labelText: 'Your address',
        hintText: 'e.g: 19 Nguyen Huu tho, phuong Tan Phong, Q7',
      ),
      validator: (value) {
        if (value != null) if (value.length < 10) {
          return "Please enter a correct address";
        }
        return null;
      },
    );
  }

  Widget submitBTN() {
    // ignore: deprecated_member_use
    return RaisedButton(
      color: Colors.blue,
      child: Text('Submit'),
      onPressed: () {
        print(formKey.currentState?.validate());
      },
    );
  }
}
