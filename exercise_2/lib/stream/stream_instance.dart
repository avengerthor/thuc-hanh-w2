import 'dart:math';

import 'package:flutter/material.dart';

class WordsStream {

  Stream<String> getWords() async* {
    final List<String> words = [
      'Dog',
      'Cat',
      'Human',
      'Love',
      'User',
      'Passenger',
      'Book',
      'Dictionary',
      'Elevator',
      'Navigator',
    ];

    yield* Stream.periodic(Duration(seconds: 3), (int t) {
      int index = Random().nextInt(10);
      return words[index];
    });
  }
}