import 'package:flutter/material.dart';
import '../stream/stream_instance.dart';

class App extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }

}

class HomePageState extends State<StatefulWidget> {
  Color bgTextColor = Colors.blueGrey;
  String? word;
  int count = 0;
  // ColorStream colorStream = ColorStream();
  WordsStream wordsStream = WordsStream();

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Stream Words',

      home: Scaffold(
        appBar: AppBar(title: Text('Stream Words'),),
        backgroundColor: bgTextColor,
        body: Container(
          child: Column(
            children: [
              Text("Welcome to Stream Words"),
              Text("Number of text received= $count"),
            ],
          ),
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.play_arrow),
          onPressed: () {
            changeWord();
          },
        ),
      ),
    );
  }

  changeWord() async {
    wordsStream.getWords().listen((eventWord) {
      setState(() {
        print(eventWord);
        // bgTextColor = eventWord;
        word = eventWord;
        count++;
      });
    });
  }
}